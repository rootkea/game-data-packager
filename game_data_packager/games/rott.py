#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2014 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import logging

from ..game import (GameData)

logger = logging.getLogger(__name__)

class ROTTGameData(GameData):
    def add_parser(self, parsers, base_parser):
        parser = super(ROTTGameData, self).add_parser(parsers, base_parser)
        parser.add_argument('-f', dest='download', action='store_false',
                help='Require 1rott13.zip on the command line')
        parser.add_argument('-w', dest='download', action='store_true',
                help='Download 1rott13.zip (done automatically if necessary)')
        return parser

GAME_DATA_SUBCLASS = ROTTGameData
