#!/bin/sh
# Copyright © 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

if ! command -v reuse >/dev/null; then
    echo "1..0 # SKIP reuse not found"
    exit 0
fi

export LC_ALL=C.UTF-8
echo "1..1"

if [ -n "${GDP_SRCDIR+set}" ]; then
    cd "$GDP_SRCDIR"
fi

if reuse lint >&2; then
    echo "ok 1 - REUSE-compliant"
else
    echo "not ok 1 # TODO not REUSE-compliant"
fi
