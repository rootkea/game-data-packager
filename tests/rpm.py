#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2016 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import sys
import unittest

if 'GDP_UNINSTALLED' not in os.environ:
    sys.path.insert(0, '/usr/share/game-data-packager')
    sys.path.insert(0, '/usr/share/games/game-data-packager')

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from game_data_packager.data import (PackageRelation)
from game_data_packager.packaging.rpm import (RpmPackaging)

class RpmTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_relation(self):
        # a random distribution that we don't actually support
        rp = RpmPackaging('caldera')

        def t(in_, out):
            self.assertEqual(
                    sorted(rp.format_relations(map(PackageRelation, in_))),
                    out)

        t(['libc.so.6'], ['libc.so.6'])
        t(['libc.so.6 (>= 2.19)'], ['libc.so.6 >= 2.19'])
        t(['libjpeg.so.62'], ['libjpeg.so.62'])
        t(['libc.so.6', 'libopenal.so.1'], ['libc.so.6', 'libopenal.so.1'])
        t([dict(deb='foo', rpm='bar')], ['bar'])
        t([dict(deb='foo', rpm='bar', generic='baz')], ['bar'])
        t([dict(deb='foo', generic='baz')], ['baz'])
        t([dict(deb='foo')], [])
        t([dict(rpm='foo', caldera='bar', fedora='baz')], ['bar'])

        with self.assertRaises(AssertionError):
            rp.format_relation(
                    PackageRelation('libopenal.so.1 | bundled-openal'))

    def tearDown(self):
        pass

if __name__ == '__main__':
    from gdp_test_common import main
    main()
