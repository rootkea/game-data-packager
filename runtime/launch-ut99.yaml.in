---
binary_only: true
required_files:
  - System/Core.so
  - System/Core.u
  - System/UnrealTournament.ini
  - Maps/Entry.unr
base_directories:
  - $prefix/lib/ut99
  - $assets/ut99
  - $assets/unreal-ut99-shared
dot_directory: ~/.utpg
old_dot_directories:
  - ~/.loki/ut
working_directory: $prefix/lib/ut99/System
library_path: [$prefix/lib/ut99/System]
argv: [$prefix/lib/ut99/System/ut-bin, "-log"]
symlink_into_dot_directory:
  - System
copy_into_dot_directory:
  - System/*.ini
edit_unreal_ini:
  System/UnrealTournament.ini:
    once:
      # Replace Windows paths with Unix paths
      - section: Core.System
        replace_key:
          SavePath: ../Save
          CachePath: ../Cache
        delete_matched:
          - "Paths=..\\System\\?.u"
          - "Paths=..\\Maps\\?.unr"
          - "Paths=..\\Textures\\?.utx"
          - "Paths=..\\Sounds\\?.uax"
          - "Paths=..\\Music\\?.umx"
      # Security hardening: remove the UDP "beacon" (announcement to LAN),
      # server query (fetching details from server) and "uplink"
      # (announcement to a master server). Probably only UdpServerQuery
      # was actually vulnerable, and hopefully the 469b unofficial patch
      # is no longer vulnerable, but it seems best to be cautious.
      - section: Engine.GameEngine
        comment_out_reason: >
          commented out to prevent exploitation of security vulnerability
          CVE-2004-0608 <http://aluigi.altervista.org/adv/unsecure-adv.txt>
        comment_out_matched:
          - ServerActors=IpDrv.UdpBeacon
          - ServerActors=IpServer.UdpServerQuery
          - ServerActors=IpServer.UdpServerUplink
          - ServerActors=IpServer.UdpServerUplink *
      # Enable Bonus Packs for use and auto-download by default
      - section: Engine.GameEngine
        append_unique:
          - ServerPackages=epiccustommodels
          - ServerPackages=multimesh
          - ServerPackages=relics
          - ServerPackages=de
          - ServerPackages=tcowmeshskins
          - ServerPackages=tnalimeshskins
          - ServerPackages=tskmskins
          - ServerPackages=SkeletalChars
    always:
      - section: Core.System
        append_unique:
          - Paths=../System/*.u

          - Paths=../Maps/*.unr
          - Paths=$prefix/lib/ut99/Maps/*.unr
          - Paths=$assets/ut99/Maps/*.unr

          - Paths=../Textures/*.utx
          - Paths=$prefix/lib/ut99/Textures/*.utx
          - Paths=$assets/ut99/Textures/*.utx
          - Paths=$assets/unreal-ut99-shared/Textures/*.utx

          - Paths=../Sounds/*.uax
          - Paths=$prefix/lib/ut99/Sounds/*.uax
          - Paths=$assets/ut99/Sounds/*.uax
          - Paths=$assets/unreal-ut99-shared/Sounds/*.uax

          - Paths=../Music/*.umx
          - Paths=$prefix/lib/ut99/Music/*.umx
          - Paths=$assets/ut99/Music/*.umx
      # We package everything needed for the Game of the Year edition
      - section: UTMenu.UTLadder
        replace_key:
          bGOTY: "true"
