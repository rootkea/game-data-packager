#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

# pre-calculate a list of packages from yaml files
# for bash auto-completion

import os
import glob

srcdir = os.path.dirname(
    os.path.dirname(
        os.path.abspath(__file__)
    )
)

for yaml_file in sorted(glob.glob(os.path.join(srcdir, 'data', '*.yaml'))):
    print(os.path.splitext(os.path.basename(yaml_file))[0])
    with open(yaml_file, encoding='utf-8') as raw:
        for line in raw:
            if line.strip() == 'packages:':
                break
        for line in raw:
            if line == '\n':
                continue
            if line[0] != ' ':
                break
            if line[2] not in (' ', '#'):
                print(line.strip(':\n '), end=' ')

        print('')
