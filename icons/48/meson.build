# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

custom_target(
  'quake3.png',
  build_by_default : true,
  input : meson.project_source_root() / 'icons' / 'quake3-tango.png',
  output : 'quake3.png',
  command : [
    'convert',
    '-resize', '48x48',
    '@INPUT@',
    '@OUTPUT@',
  ],
  install : true,
  install_dir : get_option('datadir') / 'icons' / 'hicolor' / '48x48' / 'apps',
)

custom_target(
  'quake3-team-arena.png',
  build_by_default : true,
  input : meson.project_source_root() / 'icons' / 'quake3-teamarena-tango.png',
  output : 'quake3-team-arena.png',
  command : [
    'convert',
    '-resize', '48x48',
    '@INPUT@',
    '@OUTPUT@',
  ],
  install : true,
  install_dir : get_option('datadir') / 'icons' / 'hicolor' / '48x48' / 'apps',
)

foreach icon : quake_icons
  custom_target(
    icon['name'] + '.png',
    build_by_default : true,
    input : icon['svg'],
    output : icon['name'] + '.png',
    command : [
      'inkscape',
      '--export-area=0:0:48:48',
      '--export-width=48',
      '--export-height=48',
      '--export-id=layer-' + icon['game'] + '-48',
      '--export-id-only',
      '--export-filename=@OUTPUT@',
      '@INPUT@',
    ],
    install : true,
    install_dir : get_option('datadir') / 'icons' / 'hicolor' / '48x48' / 'apps',
  )
endforeach
