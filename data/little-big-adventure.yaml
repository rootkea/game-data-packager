---
longname: Little Big Adventure
franchise: Little Big Adventure
copyright: \u00a9 1994 Electronic Arts / Adeline Software International
engine: scummvm (>= 2.5.0)
plugin: scummvm_common
gameid: twine:lba
wiki: Little_Big_Adventure
gog:
  url: little_big_adventure

packages:
  little-big-adventure-data:
    langs: [en, de, es, fr, it]
    install:
      - assets
      - music flac
      - video
      - lba.cfg
      - lba_gri.hqr
    doc:
      - manual.pdf
      - readme.txt
    license:
      - licenses

files:
  # GOG.com installer (Windows)
  setup_little_big_adventure_1.0_(28186).exe:
    provides:
      - assets
      - cd image
      - lba.cfg?gog
      - lba_gri.hqr?gog
      - manual.pdf
      - readme.txt?gog
      - licenses
    unpack:
      format: innoextract

  lba.cfg:
    alternatives:
      - lba.cfg?cd
      - lba.cfg?gog

  lba_gri.hqr:
    alternatives:
      - lba_gri.hqr?cd
      - lba_gri.hqr?gog

  readme.txt:
    alternatives:
      - readme.txt?cd
      - readme.txt?gog

groups:
  archives:
    group_members: |
      438657152 43d4926dc8a56a95800e746ac9797201 setup_little_big_adventure_1.0_(28186).exe

  # The GOG version contains a CD image of the original game.
  # (LBA.DAT is a cue sheet and LBA.GOG contains the raw image data.)
  # Although ScummVM can identify the GOG version via LBA.GOG [1], it currently
  # (version 2.5.1) does not seem to be able to use the CD image's content, as
  # it neither plays the movies nor the CD music tracks.
  #
  # The contained ISO image and audio tracks can be extracted with bchunk like
  # follows:
  #
  #   $ bchunk -w LBA.GOG LBA.DAT track_
  #
  # The ISO image contains the *.fla video files that are missing from the GOG
  # installer, but lacks the *.vox sound files that are only included in the
  # GOG installer.
  # In order for ScummVM to be able to use the extracted audio tracks, they
  # need to be converted to either FLAC or Ogg Vorbis and their file names need
  # to follow a certain scheme [2].
  #
  # [1] https://github.com/scummvm/scummvm/blob/7fa0835be7d12b1115f5f572f0d22c4ef57f7fc3/engines/twine/detection.cpp#L431-L478
  # [2] https://github.com/scummvm/scummvm/blob/7fa0835be7d12b1115f5f572f0d22c4ef57f7fc3/doc/docportal/use_scummvm/game_files.rst#cd-audio
  #
  cd image:
    group_members: |
      438       5f682c302f4f54136e0c7101cf4c6806 lba.dat
      380666496 7ee4e229c66b69480d16e6280a1af603 lba.gog

  misc:
    group_members: |
      3045      d2179be6bd8422e4c7166f50a47a2023 lba.cfg?cd
      3084      60f1e0009b7e3d162a5ba0f6df3494b2 lba.cfg?gog
      601645    a0cb2a9b6f09f3bcf94946f99a0526e2 lba_gri.hqr?cd
      602001    a596bda010cf6056c831b4629a38033e lba_gri.hqr?gog

  assets:
    group_members: |
      128603    89668bdb0ea7be17385cd70285cb3641 anim.hqr
      396854    c8f1a6ebf627c37d8aa6393dd694e624 body.hqr
      11851     0f5a6e6734b5d46264e47d50abe33726 file3d.hqr
      38059     3c9dd3be96ba573875be6f5ba7933eb2 invobj.hqr
      55785     7ec98c710855bdeca25b005bd9ad018a lba_bll.hqr
      3977143   3e6d63886fb53704a8967d0bb6264c27 lba_brk.hqr
      72870     e1817ab8ed6fdaeaff72a52a1460319b midi_mi.hqr
      55622     dd3599dc3f4eac234065cb7b3173c66c midi_sb.hqr
      258513    58275cbf4760521eff5f87db21765bb7 relent.exe
      954401    b0a75569c52cc03fea8d7fd2dbe22d9f ress.hqr
      2429133   bf81c16a551de731bc010685d1ad99f5 samples.hqr
      179725    28d383db3202e0814497c973e46aa05d scene.hqr
      180143    decc5ee6fe65fd7ba7a126e41c347331 sprites.hqr
      248654    e8e66634e31ded587544c5394a985079 text.hqr
      6212701   0591955b3010863d8d72de6c1feab37a vox/de_000.vox
      10105311  d7be323f5537e7bbef76fac49a598ca6 vox/de_001.vox
      1731494   a7c237302d9c6cc0569b1a7d67e848df vox/de_002.vox
      4253383   6d363884ce0c0850d9e00368e6e1c1a8 vox/de_003.vox
      1366631   f0c49d25765f822ddd75e3ddc032e822 vox/de_004.vox
      1868032   70c84f81436ed62af1e64f7d6d602c9c vox/de_005.vox
      1066193   cfe7ad9035d97ed6ca97270f2da2e678 vox/de_006.vox
      2321866   fc9b87f45dc394c3327f6b4edb96a478 vox/de_007.vox
      287489    ddc5048b4d9077069c026f87fbbcee95 vox/de_008.vox
      1626460   c98e954de80b0b5578e6415ad831a1d0 vox/de_009.vox
      967875    bbf43f0156c225f1db8d4be2755b9b4e vox/de_010.vox
      1824842   beb3ed9ffb1e992b4ad6f5ee95d63e1a vox/de_gam.vox
      6249564   dc582eb50a00117fffebdccf6d754e17 vox/en_000.vox
      10343661  5c420beba82bc4974cccaa6c35be0a3c vox/en_001.vox
      1844083   0ee9724ebd5acfdf250bb8e47c0ac75a vox/en_002.vox
      4022734   1460e3760a324ddc9691ab7b995cb092 vox/en_003.vox
      1367563   6b23b74fdf0fc9ebce71a26ad1f02bfd vox/en_004.vox
      1760125   64c1468753fbf84e27cff46cb86cec61 vox/en_005.vox
      1122810   4b575b1daa38fc7937002b6adcce0085 vox/en_006.vox
      2164787   2edf33b3fedf93ea66ceb9b9e5c0567d vox/en_007.vox
      284547    7315ae54d01d733f25c295434d8360bc vox/en_008.vox
      1800800   1f1824a5d30a7f63845a53aeabb77119 vox/en_009.vox
      813908    5430f2d6d763101b0087b6019e0b6349 vox/en_010.vox
      1922674   a34ad58b76fd9aa8ce27142757c9afe4 vox/en_gam.vox
      6516645   d453395b43a9a01165a7420ce512bfc1 vox/fr_000.vox
      11283116  6294093153b53ffb682c96064a34ba08 vox/fr_001.vox
      2026364   f5935d730a4efa96541ca6adddd2c1e9 vox/fr_002.vox
      4295526   59152711d6b0961627d8962fcd1a65eb vox/fr_003.vox
      1351011   2f4305eb2b85f2ff5a658f5130fbb662 vox/fr_004.vox
      1819148   fbdd84b1cd0cbc1fd17198ce6dba7083 vox/fr_005.vox
      1257886   6295bff82545f34672bda69b8e3d675a vox/fr_006.vox
      2275292   d19383865c91b6be7c19eff25698ec11 vox/fr_007.vox
      297681    3d09a5e531cf7b86e21cc40964315826 vox/fr_008.vox
      1995388   d622c3a8b5d72eaf83e3025b4e3f425a vox/fr_009.vox
      923434    a502206198f9a4993998e3453facf661 vox/fr_010.vox
      1970953   6528544bd4945de2bbf2e72384ec596e vox/fr_gam.vox

  # The audio tracks from the CD image converted to FLAC.
  music flac:
    group_members: |
      17256080  64eb9f20678251b21171e5b95b2b137e track_01.flac
      15729305  7b236fbdeeef9eb9852761829356edef track_02.flac
      14133787  f5b6aa17ea52cdb8948adb5d8704d82d track_03.flac
      15148776  22dfdf027bde0688ba0d7f6b93960520 track_04.flac
      14692313  66400dcd024661ab8ca975ebc3468b6f track_05.flac
      6862493   40d0890c2e20947ca5cc9de00d6d023a track_06.flac
      18051774  776970c81c470594f42e4404b288d44f track_07.flac
      10433818  dd28ca2d38937e6cd403af25cf77f560 track_08.flac
      22709140  026997d139a50af3b078ab3352b92f5d track_09.flac

  video:
    group_members: |
      794794    d3f700509ce34faea8293733a2ca844e fla/baffe.fla
      779828    cd5a7b491df7a07c7cc3932fe47ca6e1 fla/baffe2.fla
      783058    43bec83a04deba0c954df364cb7e453d fla/baffe3.fla
      1114382   e1dcbdcddb16f805187e354e7123efdf fla/baffe4.fla
      4354566   b30209d479b132b7eb30f2aba3b2480a fla/bateau.fla
      4406328   76f1093ad571facf5d9137b82990c85b fla/bateau2.fla
      3588436   37ca09b04ac84162c21801af6241a7c2 fla/capture.fla
      6324644   38f02f2daee960b4753270d535c1fb96 fla/dragon3.fla
      5050118   bda56781d644d823d9f078f2350b5041 fla/explod.fla
      3047944   b627cfac91341c4986777f64c7553652 fla/explod2.fla
      1701951   0f835b9642519481de5f3f1fdb65367f fla/flasamp.hqr
      4257630   060d241e38cd3e8eaa98f57ba7fc80d0 fla/flute2.fla
      900702    7e17f09a84f2633920abfcb9275dab67 fla/fortress.fla
      2575978   686b7006ce82e3a65d769bae5acd474c fla/glass2.fla
      15885218  eb9c7259739391f154270d8b661f4eb2 fla/introd.fla
      1924918   62aa1c5d4f4829150bb5146f30d2aaa4 fla/navette.fla
      4287064   80140b07b75fc555e3bc685c4a028530 fla/neige2.fla
      8133672   25d9894bd162b0b6bb83972294ae975d fla/sendel.fla
      8302918   00b7a91095ba17ba33bfef9ef4b4cbae fla/sendel2.fla
      1604488   22e1cb1e05ef370a44e9079abb1e4822 fla/surf.fla
      6080144   8f5fa71b4c405b043371659b4a603f7c fla/templebu.fla
      1017252   5aece7b0f68f3acda801096d6ab70eb2 fla/the_end.fla
      835306    12c7377e77b8182c31bba0a572e90eb2 fla/verser.fla
      786178    0fa7a383c9851215313a0718434d908b fla/verser2.fla

  documentation:
    doc: true
    group_members: |
      2365113   87683f4cc434df41bb5383121802196c manual.pdf
      8269      791bd287d04a5cc5a3382b4f2817f743 readme.txt?cd
      3576      4c866bb7b00f286be99776e2ba413c35 readme.txt?gog

  licenses:
    group_members: |
      36755     d956462aed1a0e2bce34d1b60abfca37 eula.txt
      43163     3ab82c74ab2fa83a173f634e78a46942 eula_de-de.txt
      36755     d956462aed1a0e2bce34d1b60abfca37 eula_en-us.txt
      45134     695caacdc4fe96b9b02302c663fe7a9e eula_fr-fr.txt
      39514     6f543bb234f2f7ae4a6716944af345e2 eula_pl-pl.txt
      41049     756115e3e6b96ad57148eb6e306dc260 eula_pt-br.txt
      73635     e468783b617fbd93b7bca9a9b96896c1 eula_ru-ru.txt
      35129     cfd5c510c75ebda3ec28fd764e9577d9 eula_zh-hans.txt
    license: true

sha1sums: |
  f96f28de005fedf102dae4b1a9a4afe5addcb8ff anim.hqr
  f254c49ecde7977f7bc549409132df9bac5431db body.hqr
  e39c93bf6297dfd9c5c2b758d7ed44f5192f7a39 eula.txt
  d54bd03e2669deee4c396d99de95eba9b8218ba3 eula_de-de.txt
  e39c93bf6297dfd9c5c2b758d7ed44f5192f7a39 eula_en-us.txt
  1debe3e56c44e8e2e5991d3c0123bd398ae5c4d5 eula_fr-fr.txt
  03dd481ba3aaea7b20980c928c93551e7e8e3687 eula_pl-pl.txt
  e415ee11ff34d0e39fb4904ac693de69c025a3b3 eula_pt-br.txt
  0148f80fa404add34331cfc5412d5651dbd98a12 eula_ru-ru.txt
  74c59ce79c2523e177a5c7ab5dd272fc7a6ef07b eula_zh-hans.txt
  9ec723e3c1ca6f04d65eedebe69f4d6c449145d7 file3d.hqr
  229d9bcea1bd3858d8cfc6c662a7a41536ffe409 fla/baffe.fla
  e4a964a5494acda501a20a5cba75518f0fdf55ba fla/baffe2.fla
  6846e137a59a0ecd33f306b74b251b0d56d1e330 fla/baffe3.fla
  3d38fc2e1ab01d226ec76bcd73ad96c1a5175ec5 fla/baffe4.fla
  c32a76c3d0442c517995ac85a9e1bccfbd4651de fla/bateau.fla
  236b5c607729acaef63d1ff5b87c5bd950ab9b63 fla/bateau2.fla
  77e8a3fc34caeb5884167ee62482830cf8067e95 fla/capture.fla
  ee32a0aead7cc3b35703ccc77b6e05e49c3e61f0 fla/dragon3.fla
  7860f6f5ca12fc49c7ed919ba033fd7cc45ce34d fla/explod.fla
  f7006f1ef125005085f5411820fbcb147d2e3c86 fla/explod2.fla
  9006617dab9ae36fe3d66b8150c167c0e06b1052 fla/flasamp.hqr
  f5f562b590ddc8bfe10b840a6ab09e1d780b7e8c fla/flute2.fla
  ac0f97bb2612327dd7293413a6377bc5423a4130 fla/fortress.fla
  38a9c6fc3a728c9def40cb2eb97156125216da1d fla/glass2.fla
  802d2b02b33134332dd4180f4a7709595fdaf449 fla/introd.fla
  3058ad8b4d13caa0b0b5a10003b26db140aff5b7 fla/navette.fla
  b445a54668225d36c395a64a8dbe1d334f7ebadd fla/neige2.fla
  6715d0d2c67183b35e94142508141e1dc332040a fla/sendel.fla
  fab6e6d57ac5efef8624d5c3afdb0951f75c33b5 fla/sendel2.fla
  411b372cc56df3012b93b86c1787a7c094217b81 fla/surf.fla
  1e05a292c7ec6420f428989cbde6ba008b7a29ee fla/templebu.fla
  4462ec49245b2d02d27f4ee13dfff85cef0b6537 fla/the_end.fla
  4b578e94d220518b61fd75a1e88d4bddeadec9a4 fla/verser.fla
  9405c62bf1849263fd4b7c60366b633a9df7d0ab fla/verser2.fla
  cf70497226209126e4e2711401513aa8fcb5f581 invobj.hqr
  f9a721d3684d48749c4332e8b7b667499fba8ab4 lba.cfg?cd
  ecca0e691383d61eefbc32be486b3dfeca85b237 lba.cfg?gog
  82a15a7b84e7babe62f413ab9b583ab5707089d6 lba.dat
  833fe93d9fba5b83d44b4807ca14e3bf0f777459 lba.gog
  eea55d055ca18a9fbb2987b43caa8a4c7d3da19e lba_bll.hqr
  d7b794ca011a8d198828a284b967eca5cba516f2 lba_brk.hqr
  ed60b4995dd1d6c62e3c49aa8a00fe6771c06361 lba_gri.hqr?cd
  33d04cda8a4fdbc3c1e1b1af2ce7839f9a3e149a lba_gri.hqr?gog
  2391637ba2621e56bf50256c3fbc5b7af76bb024 manual.pdf
  fff2b27e95d5333d652fbd4bcab6cf9c90da2147 midi_mi.hqr
  72deca406a0c263502119883e77318c6719f24a9 midi_sb.hqr
  762e2f778117245f769172422b790d4c64832c87 readme.txt?cd
  6626b59c9cd620f39f33f4b7a422c7bfec92bd5d readme.txt?gog
  f693fb8ad48f1da2aa58939829010d94310d0959 relent.exe
  892a40b06ea4a87577ceca6a36addc49f8a82b40 ress.hqr
  cb21aa97e2bdce4d8030922cfbaa5d124c2b7b8b samples.hqr
  1b203108dce6e4a73e4659e38cf077f25943ea79 scene.hqr
  89944ab876b33dc776f7ba6700d70e51b0212949 setup_little_big_adventure_1.0_(28186).exe
  53972d0c8a0980f72a848188980e2214e3aad760 sprites.hqr
  80f1a6482809d7f8038c3a4c48d214eb406d6bf4 text.hqr
  481c72ec286df0869634348742cbae3e3be5ae48 vox/de_000.vox
  9f356c87e8da42813ad7c7a6ff8e19fb5aea0921 vox/de_001.vox
  77eab065dbde61c6f9462555c22b84b49747bc18 vox/de_002.vox
  6bedf5b3f79f5f3ffa3f54234fdc8e34d085f2d0 vox/de_003.vox
  fb3738550f86119391d807129704bd414a0ac600 vox/de_004.vox
  5a25cfeda76555b22d7e58821615f50757e51cc2 vox/de_005.vox
  57e64d866c048c92a7bdffdf1a046af23f930960 vox/de_006.vox
  905fa802ee4b6d14ec9f266b61bd5a5988e32654 vox/de_007.vox
  8d576d95610f2d78262ea0762e923f55de9085a7 vox/de_008.vox
  58e50049e49390810c6c52d8d6c4f0043a450d0d vox/de_009.vox
  67a954dcb4022218d9a5f33035152000ef1e21e6 vox/de_010.vox
  2c19440f4415b1294c1060d387f660a11b5e60f2 vox/de_gam.vox
  f585169fa17a3c6a8655a71edc6d566ae9d98755 vox/en_000.vox
  255ad9963fe657a440807899dbde41bc35d52a79 vox/en_001.vox
  c7dfec029bf5b07707f8973be1f7c81fe449a895 vox/en_002.vox
  d4696f264b748b9227144b2a391623ae2e2e7128 vox/en_003.vox
  09fe9ecf06b1a399faf2b5f2d7a74797691420bb vox/en_004.vox
  ade9ed46304e3e7adbd68e5d386f19b999365249 vox/en_005.vox
  b1c1313e2214b3c47ddc43dab8fc67d57f54b1de vox/en_006.vox
  624634c8d87fd3fb0feaabc7a77500b0eb492e05 vox/en_007.vox
  8223a77007b3b497ab997e9af3546166e80c3a59 vox/en_008.vox
  e2f952e72459b8e8e19c4baa15dcbd700349d983 vox/en_009.vox
  47d181039f65bbe97d6558a798769216b742d24c vox/en_010.vox
  9b719e1b8ec70a58f26c4d6d51224a698e8fbf02 vox/en_gam.vox
  d5089e29991c24fc48e6bff255c4a56eeffe49df vox/fr_000.vox
  255f4e465db1b8865a4167ee5b6aabf0dc63af40 vox/fr_001.vox
  0adaf6de29ffca3956df17fa7d661c7a768613f8 vox/fr_002.vox
  ab5b23ef7e0738999539fd1ca6ee3ea4c609c5ce vox/fr_003.vox
  59dac9b310a6acdd5ba3529b02aef0b098370985 vox/fr_004.vox
  07eca6d8ac41c5151477d96f1ca23a32d6ad57a9 vox/fr_005.vox
  d59d50c090f6a130503da2efdc17449d0eb8babc vox/fr_006.vox
  733d5d428273e3916750dc37cad06d19b6dff25c vox/fr_007.vox
  e7baf84300f2a75d35af527537a665062f52c7dc vox/fr_008.vox
  c089020abd155df6c51e080d1386311b2eed4ff8 vox/fr_009.vox
  20ae318f53e6923c7b876c751488faa479fe6da3 vox/fr_010.vox
  bfdf1d955d8fbd5f12edb4398a7be0cf58003a22 vox/fr_gam.vox

sha256sums: |
  dfb2178ad4dde977b4cf9e08c8ab5ed8eef85c55c47564cc6cc19b5329a8cc23 anim.hqr
  3100636a705aadf5d2650759111504fb0d923b3855ccccf027b94d211336bd49 body.hqr
  92f8b49d186c49c154bee5eae025934a896b26a0836ae3147e0488c2a1eb3a2f eula.txt
  d1cfe320cb6081bdace9398d7bfb7878ef84a81bd1dc24648133c97171e0be68 eula_de-de.txt
  92f8b49d186c49c154bee5eae025934a896b26a0836ae3147e0488c2a1eb3a2f eula_en-us.txt
  3efaccd7a6adec010c92646d3d140c54fafb05318a1e923278086c04c484b5f1 eula_fr-fr.txt
  4a076da0a7a713665ed42fe0a8be6bab13083c43ffff121b8b829009a9c86f74 eula_pl-pl.txt
  8088039c555792603480bcc09546a1570053bd7ead5263931deeb9eca9896421 eula_pt-br.txt
  20756e9f49e29f3300925c509b4b05b9482a42e3738297b98007a6eae7a12a40 eula_ru-ru.txt
  5c7c671b4c43beaae272bd73b5fd1036137f5215bd8b9352409adc997f92653d eula_zh-hans.txt
  74abc1a90f1543dc0c2d8141ba7b0c6f773c29b71ab8e782910bd31103617d2d file3d.hqr
  c9381191173073347e25c2a848ae86f55a457d24d2afce9ec26bc650f392bc83 fla/baffe.fla
  df62e9ab83e53b89d7a89e2377b85e6da140f4fd15908e6f182bab0d044be32f fla/baffe2.fla
  9fc43436b597f138bc90871ec643213b6e54ab51eb8a0e2d0852f78ce251d114 fla/baffe3.fla
  e8fc21edc0fbc6045705312b4a25ac990b4b0e2d5b61f54a17b2964e321aa890 fla/baffe4.fla
  43162719452297710b946a8ce69fcdbfff849139e8a3d1feb24fe0a3b840f98a fla/bateau.fla
  7c7ee13bbfe8008696660d798e1d699a0702664bd60be2514f29b207ad8b23b4 fla/bateau2.fla
  c0d7419a690ec8ba8590a83df74d20f83b58bbe1e19ae752b960141d4e91d725 fla/capture.fla
  71a65dd32d0fb1bcb17d0b011fa21f34712d1ee3aa3f5f9005df1aeed2cd7438 fla/dragon3.fla
  381300330e50d8cd202c35a619a6450e51afc5ba3f62fb27994500b84b1a44f3 fla/explod.fla
  e436750ff2d40ab0e69326f97cd39d2cfc9bd2eb220c4af1346d1b9a5b184254 fla/explod2.fla
  bafd0f7e7d00fa546b8d830832b49e25a0295d9196917ed1dd87233b31802cd5 fla/flasamp.hqr
  574d2027e02faca2324bcb5241435959a968dfec5eafb90b8541080d63439829 fla/flute2.fla
  d2cfaad1bd524fa8ebf2cb6971bb5c99622bc3e689cb20207f2504e81dae7566 fla/fortress.fla
  82777b3226171a5f8312077fe34439a5e7f3b5ce4dafec669eece8ae697d95aa fla/glass2.fla
  ea9febca429421cce5cff752fffd9770325dc9a72ca81b1006b941e2ca66ada9 fla/introd.fla
  151cac6d1b0712e00c3ef9b32a7a946bd982c55f22cab54a182c4dcfd655465f fla/navette.fla
  0c04b8fa7f1bb0f0bb3a6010f5a93f8d1790160699b8b71ceabcfed1e00a2c37 fla/neige2.fla
  d3c94de4ba92833a0283f7177bc884b81e77cbf83df96b374fb0080eee43517b fla/sendel.fla
  9a38bcbde056258d03c35fb004f700b12b96a136211a7461cb6864cfe5c24215 fla/sendel2.fla
  a5487ad74454ff48d6b791be7e19d2a03f18a1be5b093b8c2f4482e6004d1906 fla/surf.fla
  234c60c2ac901dfa9ecbcc5ddacdc57de89fb26842252414cc25c194af270054 fla/templebu.fla
  1aa4dfea137a0f338fa4dd36c4f7d641d9d0013096cf09f0ae67ecec9b356578 fla/the_end.fla
  b4e9d86b91f59d292042c1e09e17b90951f6711b704c96698de159587c4c1be3 fla/verser.fla
  e49b82efe20e1898b1dce669afc9a5a11572365838b0798231969c9bbac958ff fla/verser2.fla
  fb43cd0f57de15b7a2f13745a7900b3c471a817dc1e998cae152a43968cd42cb invobj.hqr
  156a1d9391ad5957dc37c3fb74a323c87415b772985812654e1bdaa96394cf21 lba.cfg?cd
  6759ac3b429527332c6f3544506e6343cca318701bdd36c3123ee65f7f7be3b4 lba.cfg?gog
  131e4947c77f85141fbf45731fd64112b0115141ab3580edb23252c148e3bbb7 lba.dat
  1d3a8b4260b268bebe2951d8266009fb353f1d0ae43dad566717400eb38c89bd lba.gog
  e4f101ca3a746d27376cb171fc3c790c0ae0e991426f5321677eff3c8ac59f4c lba_bll.hqr
  26cfb2564b81488fde887bf25b3c94159c0e933e4ab63175cd4ca6493b0ff13e lba_brk.hqr
  d76cd521e08a3eb28ec43926d0db81e3abee89355ae9a071bdb9a7b1684a2319 lba_gri.hqr?cd
  6e9a83405393d45858372e0aadca675735d885135ce69092257a040a95f5506f lba_gri.hqr?gog
  3cda6c01fc77bb7fb732eb554ca1c97d618eb7d7b48d431c25121765db4245e2 manual.pdf
  f7dbc4f5d852558aaf5e37a9407cbcf5ea1c1c4342cdb8467de0f01581418239 midi_mi.hqr
  0076e3f41aa082e6950ad497762a2a5df69d046a5b82c6141dabed1c4324bda4 midi_sb.hqr
  c7370748eb904ddc0bb93c08e2705b708ad5021dae0f1c74c12e8ed509c7ef7a readme.txt?cd
  fa2a5200ec7e5554067cded79449bddc8f030272cdff404ea9d696c474cb45b6 readme.txt?gog
  7bb02e35f5811c38223adeceb6602a9141a1838a981da7561153424c4d867fcf relent.exe
  40fda5776f3f0da976595d47722aed5a8cf25d8536b1064c9851a8f5f0631267 ress.hqr
  28fb96895d8177cdce2d52cb4b0d6112ae0a79346ffac321781d98cd98b290c2 samples.hqr
  a320c359fd41ef2b1be93f885a840615c316c83be50e5e81e90f93dbb647c859 scene.hqr
  b7b9cd7d2e79bc385fc1591b5f63f1de83f9c843e97c8441b9fdc32c815e2835 setup_little_big_adventure_1.0_(28186).exe
  425f7486a652085b04ed5ca2c214475255015f20764d7476117b6bac4a9e9780 sprites.hqr
  ec60aa7323a2e8daa270bda71db9db1dff4062b2048c37dc7ff9f6c641667cd2 text.hqr
  0e807d183bad6af7b7d6e68ee450b37fc0102f2187f220657cb6e790c3b6a2ac vox/de_000.vox
  6c28015b83c49be8791917090338aa3e570a9c459c910a8714b8d0a1ba477da1 vox/de_001.vox
  7c43e6523c254ba16ee880ecd105402c8beb9aa16960f8b55a8d88bddafdeb62 vox/de_002.vox
  8bf7b015775c2e66082f71deee2d077e3d94874a952c7062c9cfd0b46dc3dd4c vox/de_003.vox
  51f8b4c74895f222bc091b797a4562cc1e1e46298f1f942febadeaf567817694 vox/de_004.vox
  0ee7026f7a24cce4b9443a5547a7021280a1d0f736188d7a1f53b7a828468f84 vox/de_005.vox
  8f0bcb8e94c724bbc5a6076b56aca39ece6d0253aa7fe9572e3a9a57b0cd94f1 vox/de_006.vox
  b69ceca10d449e909d2569f51ac6715877a1f0fa6e76450a8de9a54f241a6519 vox/de_007.vox
  d569bd6791ec11b34f4087ff592663e48d18d6f6f22b591e161be88e0eee28c9 vox/de_008.vox
  785bac947ab6482147b37c851ca30f80a6e100ad666d4d9775ab5dbc407635eb vox/de_009.vox
  16b6be6221e353ef53e47243818015f947082c9f580ed60d341d1ebf8fe294e8 vox/de_010.vox
  eeef481a15a544baf054e48577cfa3429adc6a8af1807710dceebb23d38ee96c vox/de_gam.vox
  0fd1feec482fd01ddbb5e830359d65a5161c819de0e2dfff7d5e077aa931228f vox/en_000.vox
  6f95dcd766496637ac262aafc3617b7ff0a1bf3365dbd4d4ec6f7f1a18aad1e4 vox/en_001.vox
  ef63ca885a138f42555973897b0146908fa8bbcc3bc56c72311c4b9a11266bcc vox/en_002.vox
  39141ec44f8a0c9c1c1b0e1f175a166db383b7412d956dcf5508b1b30b7121f1 vox/en_003.vox
  3484576a8175b363ed23216068174b70adbac7ac413e6aa30699a69305ad1675 vox/en_004.vox
  aa47b90d36ecd5f042954645913c88973de95514f21e8ad552db25ced2e0af3a vox/en_005.vox
  f717826517424cb640099c5b9447ab1030b6442f71d7c3640b7075ef3c6345df vox/en_006.vox
  853d5cc67f716fe0d82255e93b2c1269282a5290d0995e98ac71013b8eeac89c vox/en_007.vox
  260e24ac483ac267cb92fa15183c2104640aea9e6870cfc73c310e438c050634 vox/en_008.vox
  182057cb2aedac564b459a7f7c4677cd04c2dce888f0e547367ae3777a8404fe vox/en_009.vox
  1d379d83d292628c67f78c3156bc7cfbbb5026af88a4b6511e134ca2f6baa557 vox/en_010.vox
  656821c24d0f118fc6ade2f56f4f40ce841adbffb91668ba213baae9d0df1b16 vox/en_gam.vox
  c1bd8edf7c59d6daede5550e1208a174ffc696e0a6493af3752808b1092dc0e1 vox/fr_000.vox
  a0f56755682d088cb294d2fd2cb7b6ec9463b02f7a6b6d7ad820255e0da07892 vox/fr_001.vox
  cc32413cf5b1efd02c8058bdb4ec7e72264bd002c1cfbad1095495e7159c7d5d vox/fr_002.vox
  5e5b75d92284dde0f0eb0005b47ae985ceeee4caac1bb003d2ede5635d033c2b vox/fr_003.vox
  7ef253d960967a8b009faf24251b2e0efca323e71fd80b93855a15837b48f768 vox/fr_004.vox
  889bc5f0be90011923bf0e730117f1f487101c3c63a664fc73845aff6e45b6c9 vox/fr_005.vox
  ffd80a9458d6cd3b5a324654e3c7b8cf44e7f2f50e1b34b9aa9ef57e3b33807c vox/fr_006.vox
  5dfbf4bf973711a47f346e24bbc409dc67554da41b40fd93061f40574eea6f6b vox/fr_007.vox
  871d01615a04715ba2965abdadec71f267c42c9124e3427e1922a49f042e8b3d vox/fr_008.vox
  bd0801371c328b84fbefbd19b4dafc54ef3178af05c1426ff87bf89a3104d178 vox/fr_009.vox
  9b1a89d70c6713d1ce67436d396834af4cf452d64a0a6d3899f734019a7c43ca vox/fr_010.vox
  123106ea57459155074c2bdc0c5c9eb2164c565d4375ca4731ad428f64107e3b vox/fr_gam.vox

flacsums: |
  68a3415809d19d0e26cebda5458510bc track_01.flac
  65c9c462521d7a391c58a3be2960f71c track_02.flac
  b58665bd0616deccbf940a22a492b93d track_03.flac
  a5ac0dd95465fe3b30c90d385a0dd21a track_04.flac
  07448c552e82190283be5026652af686 track_05.flac
  6ffabab2e38f87f0470f7cba610466a3 track_06.flac
  7103a214e3a32c59462279706e391716 track_07.flac
  c0f8ecf6b4a4950ed57949e0da0ed4a6 track_08.flac
  abd6bc7ed82ea82f0a8091dda1b48ec3 track_09.flac

...
